﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Timers;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Management;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using Microsoft.Win32;


namespace AdvancedService
{
    public partial class Service1 : ServiceBase
    {

        /* constants to connect with MySQL server */
        const string server = "127.0.0.1";
        const string database = "user";
        const string user = "root";
        const string password = "London2012";
        const string port = "3306";
        const string sslM = "none";
        /* paths to logfile */
        const string logfileProcess = "D:\\Services\\logProcess.txt";
        const string logfileDatabase = "D:\\Services\\logDatabase.txt";
        const string logfileDatabaseMSSQL = "D:\\Services\\logDatabaseMSSQL.txt";
        const string logfileRegistry = "D:\\Services\\logRegistry.txt";
        const string logfileFile = "D:\\Services\\logFile.txt";
        /* path to file about cars */
        const string pathfile = "D:\\Services\\cars.txt";
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

            /* write the first message */
            writeLogProcess("Working of program was started!\r\n");
            writeLogDatabase("Working of program was started!\r\n");
            writeLogDatabaseMSSQL("Working of program was started!\r\n");
            writeLogRegistry("Working of program was started!\r\n");
            writeLogFile("Working of program was started!\r\n");

            /* create and launch timer (for processes) */
            Timer timerProcess = new Timer();
            timerProcess.Elapsed += new ElapsedEventHandler(readProcesses); 
            timerProcess.Interval = 10000;
            timerProcess.Enabled = true;

            /* create and launch timer (for database MySQL) */
            Timer timerDatabase = new Timer();
            timerDatabase.Elapsed += new ElapsedEventHandler(readDatabase);
            timerDatabase.Interval = 10000;
            timerDatabase.Enabled = true;

            /* create and launch timer (for database MSSQL) */
            Timer timerDatabaseMSSQL = new Timer();
            timerDatabaseMSSQL.Elapsed += new ElapsedEventHandler(readDatabaseMSSQL);
            timerDatabaseMSSQL.Interval = 10000;
            timerDatabaseMSSQL.Enabled = true;

            /* create and launch timer (for registry) */
            Timer timerRegistry = new Timer();
            timerRegistry.Elapsed += new ElapsedEventHandler(readRegistry);
            timerRegistry.Interval = 10000;
            timerRegistry.Enabled = true;

            /* create and launch timer (for file) */
            Timer timerFile = new Timer();
            timerFile.Elapsed += new ElapsedEventHandler(readFile);
            timerFile.Interval = 10000;
            timerFile.Enabled = true;

        }

        public void writeLogProcess(string mess)
        {
            /* write message to the logfile about process */
            using (StreamWriter writer = new StreamWriter(logfileProcess, true))
            {
                writer.Write(mess);
            }
        }

        public void writeLogDatabase(string mess)
        {
            /* write message to the logfile from database */
            using (StreamWriter writer = new StreamWriter(logfileDatabase, true))
            {
                writer.Write(mess);
            }
        }

        public void writeLogDatabaseMSSQL(string mess)
        {
            /* write message to the logfile from database */
            using (StreamWriter writer = new StreamWriter(logfileDatabaseMSSQL, true))
            {
                writer.Write(mess);
            }
        }

        public void writeLogRegistry(string mess)
        {
            /* write message to the logfile from file */
            using (StreamWriter writer = new StreamWriter(logfileRegistry, true))
            {
                writer.Write(mess);
            }
        }

        public void writeLogFile(string mess)
        {
            /* write message to the logfile from file */
            using (StreamWriter writer = new StreamWriter(logfileFile, true))
            {
                writer.Write(mess);
            }
        }

        public void readProcesses(object source, ElapsedEventArgs e)
        {

            /* create scope */
            ManagementScope scope = new ManagementScope(@"\\.\ROOT\cimv2");

            /* create object query */
            ObjectQuery query = new ObjectQuery("SELECT * FROM Win32_OperatingSystem");

            /* create object searcher */
            ManagementObjectSearcher searcher = new ManagementObjectSearcher(scope, query);

            /* get collection of WMI objects */
            ManagementObjectCollection queryCollection = searcher.Get();

            /* enumerate the collection */
            string answer = "";
            foreach (ManagementObject manObject in queryCollection)
            {
                /* create answer using WMI query */
                answer += ("Name:  " + manObject["Name"]).Substring(0, 56) + "\r\n";
                answer += "FreePhysicalMemory:  " + manObject["FreePhysicalMemory"] + "\r\n";
                answer += "TotalVisibleMemorySize:  " + manObject["TotalVisibleMemorySize"] + "\r\n";
            }

            /* write generated answer */
            writeLogProcess(answer);

        }

        public void readDatabase(object source, ElapsedEventArgs e)
        {

            /* rows for connection to database and query */
            string connectionRow = String.Format("server={0}; port={1}; user id={2}; password={3}; database={4}; SslMode={5}; CharSet=cp1251;", server, port, user, password, database, sslM);
            string query = "select * from users";

            try
            {
                /* make connection */
                MySqlConnection connection = new MySqlConnection(connectionRow);
                connection.Open();
                MySqlCommand command = new MySqlCommand(query, connection);

                /* read database */
                MySqlDataReader reader = command.ExecuteReader();
                string answer = "";
                while (reader.Read())
                {
                    answer += "Name:" + reader[1].ToString() + " " + "Login:" + reader[2].ToString() + "\r\n";
                }

                /* write answer to the file */
                writeLogDatabase(answer);
           }
           catch (Exception exc)
           {
                writeLogDatabase(exc.Message + "\r\n");
           }

        }

        public void readDatabaseMSSQL(object source, ElapsedEventArgs e)
        {
            try
            {
                /* rows for connection to database */
                string db = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=users;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False;";
                string query = "select * from users";

                using (SqlConnection connection = new SqlConnection(db))
                {
                    /* make connection */
                    connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    SqlDataReader reader = command.ExecuteReader();

                    /* read database */
                    string answer = "";
                    while (reader.Read())
                    {
                        answer += "Name:" + reader[1].ToString() + " " + "Login:" + reader[2].ToString() + "\r\n";
                    }

                    /* write answer to the file */
                    writeLogDatabaseMSSQL(answer);
                }
            }
            catch (Exception exc)
            {
                writeLogDatabaseMSSQL(exc.Message + "\r\n");
            }

        }

        public void readRegistry(object source, ElapsedEventArgs e)
        {

            try
            {
                /* open registry */
                using (RegistryKey tomiloKey = Registry.LocalMachine.OpenSubKey("Software\\Tomilo", true))
                {
                    /* receive password from registry */
                    string[] password = (string[])tomiloKey.GetValue("Password");
                    writeLogRegistry("Password: " + password[0] + "\r\n");
                    tomiloKey.Close();
                }
            }
            catch (Exception exc)
            {
                writeLogRegistry(exc.Message + "\r\n");
            }

        }

        public void readFile(object source, ElapsedEventArgs e)
        {

            try
            {
                string answer = "";
                /* create reader */
                using (StreamReader reader = new StreamReader(pathfile, true))
                {
                    /* read intial row of file */
                    string row = reader.ReadLine();
                    while (row != null)
                    {
                        /* add new car to answer */
                        string[] substrings = row.Split(' ');
                        answer += "Car: " + substrings[0] + " Year: " + substrings[1] + "\r\n";
                        /* read new row */
                        row = reader.ReadLine();
                    }
                    reader.Close();
                }

                /* write answer to the file */
                writeLogFile(answer);
            }
            catch (Exception exc)
            {
                writeLogFile(exc.Message + "\r\n");
            }

        }

        protected override void OnStop()
        {
            /* write the last message */
            writeLogProcess("Working of program was finished!\r\n");
            writeLogDatabase("Working of program was finished!\r\n");
            writeLogDatabaseMSSQL("Working of program was finished!\r\n");
            writeLogRegistry("Working of program was finished!\r\n");
            writeLogFile("Working of program was finished!\r\n");
        }
    }
}
